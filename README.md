# servidor impresión

# **run**

`docker run -d -p 631:631  --restart=always -v /var/run/dbus:/var/run/dbus --name cupsd registry.gitlab.com/vhuezo/servidor-impresion:latest`

**Puerto Diferente:**

`docker run -d -p 1631:631  --restart=always -v /var/run/dbus:/var/run/dbus --name cupsd registry.gitlab.com/vhuezo/servidor-impresion:latest`

[http://127.0.0.1:1631/admin/](http://127.0.0.1:1631/admin/)

**Usuario:** `print`
**Contraseña:** `print`



## **Add printers to the Cups server**
**Connect to the Cups server at**
 [http://127.0.0.1:631](http://127.0.0.1:631)
**Add printers: Administration > Printers > Add Printer**
**Note: The admin user/password for the Cups server is print/print**

**Usuario:** `print`
**Contraseña:** `print`

# run 

`docker run -d -p 631:631  --restart=always -v /var/run/dbus:/var/run/dbus --name cupsd registry.gitlab.com/vhuezo/servidor-impresion:latest`

**Puerto Diferente:**

`docker run -d -p 1631:631  --restart=always -v /var/run/dbus:/var/run/dbus --name cupsd registry.gitlab.com/vhuezo/servidor-impresion:latest`

[http://127.0.0.1:1631/admin/](http://127.0.0.1:1631/admin/)

**Usuario:** `print`
**Contraseña:** `print`


## Configure Cups client on your machine
Install the cups-client package
Edit the /etc/cups/client.conf, set ServerName to 127.0.0.1:631
Test the connectivity with the Cups server using lpstat -r
Test that printers are detected using lpstat -v
Applications on your machine should now detect the printers!

## Included package
cups, cups-client, cups-filters
foomatic-db
printer-driver-all, printer-driver-cups-pdf
openprinting-ppds
hpijs-ppds, hp-ppd
sudo, whois
smbclient